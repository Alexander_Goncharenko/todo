package com.ncube.todo.di

import com.ncube.todo.TodoApplication
import com.ncube.todo.di.module.ActivityBindingModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        AppModule::class,
        ApiModule::class
    ]
)
interface AppComponent {

    fun inject(mcmApplication: TodoApplication)


    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: TodoApplication): Builder

        fun apiModule(module: ApiModule): Builder

        fun build(): AppComponent
    }
}