package com.ncube.todo.di.module.main

import androidx.lifecycle.ViewModelProviders
import com.apollographql.apollo.ApolloClient
import com.ncube.todo.di.scope.ActivityScope
import com.ncube.todo.presentation.main.MainActivity
import com.ncube.todo.presentation.task.TaskActivity
import com.ncube.todo.presentation.task.TaskActivityViewModel
import com.ncube.todo.presentation.task.TaskActivityViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class TaskActivityModule {

    @Provides
    @ActivityScope
    fun provideViewModelFactory(
        apolloClient: ApolloClient
    ) = TaskActivityViewModelFactory(apolloClient)

    @Provides
    @ActivityScope
    fun provideViewModel(
        activity: TaskActivity,
        viewModelFactory: TaskActivityViewModelFactory
    ) = ViewModelProviders.of(activity, viewModelFactory).get(TaskActivityViewModel::class.java)
}