package com.ncube.todo.di

import android.content.Context
import android.content.SharedPreferences
import com.ncube.todo.TodoApplication
import com.ncube.todo.di.module.ActivityBindingModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ActivityBindingModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: TodoApplication): Context = application

    @Provides
    @Singleton
    internal fun provideDataPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("app", Context.MODE_PRIVATE)
    }
}