package com.ncube.todo.di.module

import com.ncube.todo.di.module.main.MainActivityModule
import com.ncube.todo.di.module.main.TaskActivityModule
import com.ncube.todo.di.scope.ActivityScope
import com.ncube.todo.presentation.main.MainActivity
import com.ncube.todo.presentation.task.TaskActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [TaskActivityModule::class])
    fun contributeTaskActivity(): TaskActivity
}