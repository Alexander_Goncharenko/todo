package com.ncube.todo.di.module.main

import androidx.lifecycle.ViewModelProviders
import com.apollographql.apollo.ApolloClient
import com.ncube.todo.di.scope.ActivityScope
import com.ncube.todo.presentation.main.MainActivity
import com.ncube.todo.presentation.main.MainActivityViewModel
import com.ncube.todo.presentation.main.MainActivityViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    @ActivityScope
    fun provideViewModelFactory(
        apolloClient: ApolloClient
    ) = MainActivityViewModelFactory(apolloClient)

    @Provides
    @ActivityScope
    fun provideViewModel(
        activity: MainActivity,
        viewModelFactory: MainActivityViewModelFactory
    ) = ViewModelProviders.of(activity, viewModelFactory).get(MainActivityViewModel::class.java)
}