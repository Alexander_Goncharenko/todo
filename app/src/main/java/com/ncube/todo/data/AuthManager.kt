package com.ncube.todo.data

import GenerateAccessTokenMutation
import android.content.SharedPreferences
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.rx2.Rx2Apollo
import dagger.Lazy
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthManager @Inject constructor(
    private val lazyApolloClient: Lazy<ApolloClient>, //to avoid dependency cycle
    private val sharedPreferences: SharedPreferences
) : ManageAuth, ReadAuth {

    override var token: String?
        get() {
            return sharedPreferences.getString(SP_TOKEN, null)
        }
        set(value) {
            sharedPreferences
                .edit()
                .putString(SP_TOKEN, value)
                .apply()
        }

    override val isSessionStarted: Boolean
        get() = token != null

    private val apolloClient by lazy { lazyApolloClient.get() }

    override fun startSession(apiKey: String, userName: String) {
        val query = GenerateAccessTokenMutation
            .builder()
            .apiKey(apiKey)
            .userName(userName)
            .build()
        val apolloPrefetch = apolloClient.mutate(query)


        Rx2Apollo
            .from(apolloPrefetch)
            .blockingSingle().let {
                if (it.hasErrors()) {
                    error(it.errors().first().message()!!)
                }

                token = it.data()?.generateAccessToken()
            }
    }

    override fun closeSession() {
        token = null
        sharedPreferences
            .edit()
            .clear()
            .apply()
    }

    private companion object {
        const val SP_TOKEN = "sp_token"
    }
}