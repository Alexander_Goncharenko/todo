package com.ncube.todo.data

interface ReadAuth {
    var token: String?
}