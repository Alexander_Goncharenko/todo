package com.ncube.todo.data


interface ManageAuth {
    val isSessionStarted: Boolean
    fun startSession(apiKey: String, userName: String)
    fun closeSession()
}