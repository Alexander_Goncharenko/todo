package com.ncube.todo.data

import com.ncube.todo.BuildConfig
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TokenInterceptor @Inject constructor(
    private val authManager: AuthManager
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        //to avoid start session recursion
        if (request.header("X-APOLLO-OPERATION-NAME") == "GenerateAccessTokenMutation") {
            return chain.proceed(request)
        }

        val builder = request.newBuilder().method(request.method(), request.body())

        //todo manage authorization error
        if (!authManager.isSessionStarted) {
            synchronized(this) {
                if (authManager.isSessionStarted) {
                    setAuthHeader(builder, authManager.token)
                    return chain.proceed(builder.build())
                }

                authManager.startSession(BuildConfig.API_KEY, "Alex4")
            }
        }

        setAuthHeader(builder, authManager.token)

        return chain.proceed(builder.build())
    }

    private fun setAuthHeader(builder: Request.Builder, token: String?) {
        token ?: error("Something went wrong during authorization!")
        builder.addHeader("Authorization", token)
    }
}