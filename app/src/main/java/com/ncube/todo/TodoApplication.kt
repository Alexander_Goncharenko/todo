package com.ncube.todo

import android.app.Activity
import android.app.Application
import com.ncube.todo.di.ApiModule
import com.ncube.todo.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class TodoApplication : Application(), HasActivityInjector {

    companion object {
        //todo move to debug build variant from build.gradle
        const val GRAPHQL_URL = "https://380odjc5vi.execute-api.us-east-1.amazonaws.com/dev/graphql"
    }

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        initDagger()
    }

    private fun initDagger() {
        DaggerAppComponent
            .builder()
            .application(this)
            .apiModule(ApiModule(GRAPHQL_URL))
            .build()
            .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingActivityInjector
    }
}