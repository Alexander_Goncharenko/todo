package com.ncube.todo.domain.model.task

import UpdateTaskStatusMutation
import io.reactivex.functions.Function

class UpdateTaskStatusMapper : Function<UpdateTaskStatusMutation.UpdateTaskStatus?, Task> {

    override fun apply(entitiy: UpdateTaskStatusMutation.UpdateTaskStatus) = Task(
        entitiy.id(),
        entitiy.name(),
        entitiy.note(),
        entitiy.isDone
    )
}