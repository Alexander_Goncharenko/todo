package com.ncube.todo.domain.model.common

enum class Status {
  LOADING, SUCCESS, ERROR
}