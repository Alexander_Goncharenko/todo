package com.ncube.todo.domain.model.task

import AllTasksQuery
import io.reactivex.functions.Function

class AllTaskMapper : Function<List<AllTasksQuery.AllTask>?, AllTasks> {

    override fun apply(entities: List<AllTasksQuery.AllTask>): AllTasks {
        val todoTasks = ArrayList<Task>()
        val completedTasks = ArrayList<Task>()

        entities.forEach {
            when {
                it.isDone -> completedTasks += mapTask(it)
                else -> todoTasks += mapTask(it)
            }
        }

        return AllTasks(todoTasks, completedTasks)
    }

    private fun mapTask(entity: AllTasksQuery.AllTask) = Task(
        entity.id(),
        entity.name(),
        entity.note(),
        entity.isDone
    )
}