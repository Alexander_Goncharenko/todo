package com.ncube.todo.domain.model.task

import CreateTaskMutation
import io.reactivex.functions.Function

class CreateTaskMapper : Function<CreateTaskMutation.CreateTask?, Task> {

    override fun apply(entity: CreateTaskMutation.CreateTask) = Task(
        entity.id(),
        entity.name(),
        entity.note(),
        entity.isDone
    )
}