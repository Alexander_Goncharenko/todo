package com.ncube.todo.domain.model.common

class Response<out T>
private constructor(val status: Status, val data: T?, val error: Throwable?) {

    companion object {
        fun loading() = Response(Status.LOADING, null, null)
        fun <T> success(data: T? = null) =
            Response(Status.SUCCESS, data, null)
        fun error(error: Throwable = Throwable()) =
            Response(Status.ERROR, null, error)
    }
}