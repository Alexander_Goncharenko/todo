package com.ncube.todo.domain.model.task

data class AllTasks(
    val todoTasks: MutableList<Task>,
    val completedTasks: MutableList<Task>
)