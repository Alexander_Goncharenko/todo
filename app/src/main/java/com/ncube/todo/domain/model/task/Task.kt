package com.ncube.todo.domain.model.task

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Task(
    val id: String,
    val name: String,
    val note: String?,
    var isDone: Boolean
) : Parcelable