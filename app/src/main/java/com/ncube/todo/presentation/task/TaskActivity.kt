package com.ncube.todo.presentation.task

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jakewharton.rxbinding2.widget.RxTextView
import com.ncube.todo.R
import com.ncube.todo.domain.model.task.Task
import dagger.android.AndroidInjection
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.ac_task.*
import javax.inject.Inject

class TaskActivity : AppCompatActivity(), AddTask, EditTask {

    @Inject
    lateinit var viewModel: TaskActivityViewModel

    private var titleDisposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        overridePendingTransition(0, 0)
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)
        setContentView(R.layout.ac_task)

        viewModel.task = intent.getParcelableExtra(ARG_TASK)

        setupTaskContainer()

        if (savedInstanceState == null) {
            setupAddTaskResponse()
        }
    }


    private fun setupTaskContainer() {
        ibClose.setOnClickListener { finish() }

        if (viewModel.task == null) {
            setupAddMode()
        } else {
            setupEditMode()
        }
    }

    override fun onResume() {
        super.onResume()

        titleDisposable = RxTextView
            .afterTextChangeEvents(tvTitle)
            .skipInitialValue()
            .subscribe { btnAdd.isEnabled = !it.editable().isNullOrEmpty() }
    }

    override fun onPause() {
        super.onPause()

        if (titleDisposable != null) {
            titleDisposable!!.dispose()
            titleDisposable = null
        }
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(0, 0)
    }

    companion object {
        const val ARG_TASK = "arg_task"

        fun createIntent(context: Context, task: Task? = null): Intent {
            val intent = Intent(context, TaskActivity::class.java)

            if (task != null) {
                intent.putExtra(ARG_TASK, task)
            }

            return intent
        }

        fun getTaskFromIntent(intent: Intent?): Task? {
            return intent?.getParcelableExtra(ARG_TASK)
        }
    }
}