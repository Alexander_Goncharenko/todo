package com.ncube.todo.presentation.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.lonecrab.multistateview.MultiStateView
import com.ncube.todo.R
import com.ncube.todo.domain.model.common.Status.LOADING
import com.ncube.todo.domain.model.common.Status.SUCCESS
import com.ncube.todo.domain.model.task.AllTasks
import com.ncube.todo.domain.model.task.Task
import com.ncube.todo.presentation.main.adapter.RvTasksAdapter
import com.ncube.todo.presentation.task.TaskActivity
import com.ncube.todo.presentation.utils.observe
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.ac_main.*
import kotlinx.android.synthetic.main.lt_main_toolbar.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)
        setContentView(R.layout.ac_main)

        setupToolbar()
        setupPullToRefresh()
        setupRecyclerView()
        setupFAB()

        if (savedInstanceState == null) {
            setupAllTasksResponse()
            setupUpdateTaskResponse()

            viewModel.fetchAllTasks()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CREATE_TASK_RESULT_CODE && resultCode == Activity.RESULT_OK) {
            val createdTask = TaskActivity.getTaskFromIntent(data)
            if (createdTask != null) {
                addTaskToAdapter(createdTask)
            }
        }
    }


    private fun setupAllTasksResponse() {
        viewModel.allTasksResponse.observe(this) {
            when (it?.status) {
                LOADING -> {
                    swipeRefreshLayout.isRefreshing = true
                }
                SUCCESS -> {
                    swipeRefreshLayout.isRefreshing = false
                    addTasksToAdapter(it.data)
                }
                else -> {
                    swipeRefreshLayout.isRefreshing = false
                    Snackbar.make(clContent, it!!.error!!.message!!, Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setupUpdateTaskResponse() {
        viewModel.updateTaskResponse.observe(this) {
            when (it?.status) {
                LOADING -> {
                    //todo
                }
                SUCCESS -> {
                    completeTask(it.data!!)
                }
                else -> {
                    Snackbar.make(clContent, it!!.error!!.message!!, Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
    }

    private fun setupPullToRefresh() {
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.fetchAllTasks()
        }
    }

    private fun setupRecyclerView() {
        var adapter = rvContent.adapter as RvTasksAdapter?

        if (adapter == null) {
            adapter = RvTasksAdapter()
            rvContent.adapter = adapter
            rvContent.layoutManager = LinearLayoutManager(this)

            //todo move logic to allTasks observer
            processTasksQuantity()
        }

        adapter.onTaskClickListener = {
            viewModel.updateTask(it)
        }

        adapter.onLongTaskClickListener = {
            val intent = TaskActivity.createIntent(this, it)
            startActivityForResult(intent, EDIT_TASK_RESULT_CODE)
        }
    }

    private fun setupFAB() {
        fabAddNewTask.setOnClickListener {
            val intent = TaskActivity.createIntent(this)
            startActivityForResult(intent, CREATE_TASK_RESULT_CODE)
        }
    }


    private fun addTasksToAdapter(tasks: AllTasks?) {
        val adapter = rvContent.adapter as RvTasksAdapter
        adapter.allTasks = tasks

        //todo move logic to allTasks observer
        processTasksQuantity()
    }

    private fun addTaskToAdapter(task: Task) {
        val adapter = rvContent.adapter as RvTasksAdapter
        adapter.addTask(task)

        //todo move logic to allTasks observer
        processTasksQuantity()
    }

    private fun completeTask(task: Task) {
        val adapter = rvContent.adapter as RvTasksAdapter
        adapter.completeTask(task)
    }

    private fun processTasksQuantity() {
        msvContent.viewState = when (rvContent.adapter?.itemCount) {
            0 -> MultiStateView.ViewState.EMPTY
            else -> MultiStateView.ViewState.CONTENT
        }
    }

    companion object {
        private const val CREATE_TASK_RESULT_CODE = 4441
        private const val EDIT_TASK_RESULT_CODE = 4442
    }
}