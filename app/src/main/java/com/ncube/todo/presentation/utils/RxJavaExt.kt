package com.ncube.todo.presentation.utils

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T : Any?> Observable<T>.applySchedulers(): Observable<T> = this.subscribeOnComputation().observeOnMain()
fun <T : Any?> Observable<T>.subscribeOnIo(): Observable<T> = this.subscribeOn(Schedulers.io())
fun <T : Any?> Observable<T>.subscribeOnComputation(): Observable<T> = this.subscribeOn(Schedulers.computation())
fun <T : Any?> Observable<T>.observeOnMain(): Observable<T> = this.observeOn(AndroidSchedulers.mainThread())

fun <T : Any?> Single<T>.applySchedulers(): Single<T> = this.subscribeOnComputation().observeOnMain()
fun <T : Any?> Single<T>.subscribeOnIo(): Single<T> = this.subscribeOn(Schedulers.io())
fun <T : Any?> Single<T>.subscribeOnComputation(): Single<T> = this.subscribeOn(Schedulers.computation())
fun <T : Any?> Single<T>.observeOnMain(): Single<T> = this.observeOn(AndroidSchedulers.mainThread())

fun Completable.applySchedulers(): Completable = this.subscribeOnComputation().observeOnMain()
fun Completable.subscribeOnIo(): Completable = this.subscribeOn(Schedulers.io())
fun Completable.subscribeOnComputation(): Completable = this.subscribeOn(Schedulers.computation())
fun Completable.observeOnMain(): Completable = this.observeOn(AndroidSchedulers.mainThread())