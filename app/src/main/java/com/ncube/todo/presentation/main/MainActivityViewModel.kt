package com.ncube.todo.presentation.main

import AllTasksQuery
import UpdateTaskStatusMutation
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.rx2.Rx2Apollo
import com.ncube.todo.domain.model.common.Response
import com.ncube.todo.domain.model.task.AllTaskMapper
import com.ncube.todo.domain.model.task.AllTasks
import com.ncube.todo.domain.model.task.Task
import com.ncube.todo.domain.model.task.UpdateTaskStatusMapper
import com.ncube.todo.presentation.common.BaseViewModel
import com.ncube.todo.presentation.common.model.ResponseLiveData
import com.ncube.todo.presentation.utils.applySchedulers

class MainActivityViewModel(private val apolloClient: ApolloClient) : BaseViewModel() {

    val allTasksResponse = ResponseLiveData<AllTasks>()
    val updateTaskResponse = ResponseLiveData<Task>()

    fun fetchAllTasks() {
        val query = AllTasksQuery.builder().build()
        val apolloPrefetch = apolloClient.query(query)

        allTasksResponse.value = Response.loading()

        Rx2Apollo
            .from(apolloPrefetch)
            .map { if (it.hasErrors()) error(it.errors().first().message()!!) else it.data()?.allTasks() } //todo make extension function to handle errors with apollo
            .map(AllTaskMapper())
            .applySchedulers()
            .subscribe(
                { allTasksResponse.value = Response.success(it) },
                { allTasksResponse.value = Response.error(it) }
            )
            .registerDisposable()
    }

    fun updateTask(task: Task) {
        val mutation = UpdateTaskStatusMutation
            .builder()
            .id(task.id)
            .isDone(!task.isDone)
            .build()
        val apolloPrefetch = apolloClient.mutate(mutation)

        updateTaskResponse.value = Response.loading()

        Rx2Apollo
            .from(apolloPrefetch)
            .map { if (it.hasErrors()) error(it.errors().first().message()!!) else it.data()?.updateTaskStatus() } //todo make extension function to handle errors with apollo
            .map(UpdateTaskStatusMapper())
            .applySchedulers()
            .subscribe(
                { updateTaskResponse.value = Response.success(it) },
                { updateTaskResponse.value = Response.error(it) }
            )
            .registerDisposable()
    }
}