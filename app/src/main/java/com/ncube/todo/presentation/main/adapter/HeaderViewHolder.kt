package com.ncube.todo.presentation.main.adapter

import android.view.View
import android.widget.TextView
import com.ncube.todo.presentation.common.BaseRvViewHolder
import com.ncube.todo.presentation.main.adapter.model.Header

class HeaderViewHolder(containerView: View) : BaseRvViewHolder(containerView) {

    fun bindView(header: Header) {
        (containerView as TextView).setText(header.titleStringRes)
    }
}