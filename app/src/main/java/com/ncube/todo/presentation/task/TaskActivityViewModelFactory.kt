package com.ncube.todo.presentation.task

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.apollographql.apollo.ApolloClient

@Suppress("UNCHECKED_CAST")
class TaskActivityViewModelFactory(private val apolloClient: ApolloClient) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TaskActivityViewModel(apolloClient) as T
    }
}