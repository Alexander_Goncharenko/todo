package com.ncube.todo.presentation.task

import android.view.View
import kotlinx.android.synthetic.main.ac_task.*

interface EditTask {

    fun TaskActivity.setupEditMode() {
        ibDelete.visibility = View.VISIBLE
        divider.visibility = View.VISIBLE
        btnAdd.visibility = View.GONE

        tvTitle.setText(viewModel.task?.name)
        tvNote.setText(viewModel.task?.note)

        tvTitle.keyListener = null
        tvNote.keyListener = null
    }
}