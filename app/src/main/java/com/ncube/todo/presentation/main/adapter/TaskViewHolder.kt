package com.ncube.todo.presentation.main.adapter

import android.view.View
import com.ncube.todo.domain.model.task.Task
import com.ncube.todo.presentation.common.BaseRvViewHolder
import kotlinx.android.synthetic.main.lt_list_task.*

typealias OnTaskClickListener = (Task) -> Unit
typealias OnLongTaskClickListener = (Task) -> Unit

class TaskViewHolder(containerView: View) : BaseRvViewHolder(containerView) {

    fun bindView(
        task: Task,
        onTaskClickListener: OnTaskClickListener?,
        onLongTaskClickListener: OnLongTaskClickListener?
    ) {
        tvTitle.text = task.name
        if (task.note.isNullOrEmpty()) {
            tvNote.visibility = View.GONE
        } else {
            tvNote.visibility = View.VISIBLE
            tvNote.text = task.note
        }

        tvTitle.isChecked = task.isDone
        taskContainer.isEnabled = !task.isDone
        tvTitle.isEnabled = !task.isDone
        tvNote.isEnabled = !task.isDone

        taskContainer.setOnClickListener {
            onTaskClickListener?.invoke(task)
            taskContainer.isEnabled = false
        }

        taskContainer.setOnLongClickListener {
            onLongTaskClickListener?.invoke(task)
            return@setOnLongClickListener true
        }
    }
}