package com.ncube.todo.presentation.common

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    private var disposables: CompositeDisposable? = null
        get() {
            if (field == null || field!!.isDisposed) {
                field = CompositeDisposable()
            }

            return field
        }

    protected fun Disposable.registerDisposable() {
        disposables?.add(this)
    }

    @CallSuper
    open fun disposeAll() {
        disposables?.clear()
        disposables = null
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        disposeAll()
    }
}