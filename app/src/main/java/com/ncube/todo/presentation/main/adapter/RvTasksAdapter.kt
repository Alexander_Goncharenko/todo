package com.ncube.todo.presentation.main.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ncube.todo.R
import com.ncube.todo.domain.model.task.AllTasks
import com.ncube.todo.domain.model.task.Task
import com.ncube.todo.presentation.common.BaseRvAdapter
import com.ncube.todo.presentation.main.adapter.model.Header

class RvTasksAdapter : BaseRvAdapter<RecyclerView.ViewHolder>() {

    private val data = ArrayList<Any>()
    private val todoHeader = Header(R.string.title_todo_tasks)
    private val completedHeader = Header(R.string.title_completed_tasks)

    var onTaskClickListener: OnTaskClickListener? = null
    var onLongTaskClickListener: OnLongTaskClickListener? = null

    var allTasks: AllTasks? = null
        set(value) {
            data.clear()

            if (value != null) {
                if (value.todoTasks.isNotEmpty()) {
                    data += todoHeader
                    data.addAll(value.todoTasks)
                }

                if (value.completedTasks.isNotEmpty()) {
                    data += completedHeader
                    data.addAll(value.completedTasks)
                }
            }

            notifyDataSetChanged()
        }

    fun addTask(task: Task) {
        val todoTitlePosition = 0
        val newTaskItemPosition = todoTitlePosition + 1

        if (data.isNullOrEmpty() || data[todoTitlePosition] != todoHeader) {
            data.add(todoTitlePosition, todoHeader)
            notifyItemInserted(todoTitlePosition)
        }

        data.add(newTaskItemPosition, task)
        notifyItemInserted(newTaskItemPosition)
    }

    fun completeTask(task: Task) {
        //todo replace with diff util
        var currentTaskPosition = data.indexOfFirst { it is Task && it.id == task.id }
        val todoHeaderPosition = data.indexOf(todoHeader)
        val completedHeaderPosition = data.indexOf(completedHeader)

        //if there is only one task remove todos header
        if (completedHeaderPosition == 2) {
            data.removeAt(todoHeaderPosition)
            --currentTaskPosition
            notifyItemRemoved(todoHeaderPosition)
        }

        //if completed tasks header does not exist add it
        if (completedHeaderPosition == -1) {
            data += completedHeader
            notifyItemInserted(data.size)
        }

        data.removeAt(currentTaskPosition)
        data.add(data.size, task)
        notifyItemMoved(currentTaskPosition, data.size - 1) //move item
        notifyItemChanged(data.size - 1) //update item state
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        HEADER_ITEM_VIEW_TYPE -> {
            val view = inflate(R.layout.lt_list_header, parent)
            HeaderViewHolder(view)
        }
        TASK_ITEM_VIEW_TYPE -> {
            val view = inflate(R.layout.lt_list_task, parent)
            TaskViewHolder(view)
        }
        else -> {
            throw IllegalStateException()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> holder.bindView(data[position] as Header)
            is TaskViewHolder -> holder.bindView(data[position] as Task, onTaskClickListener, onLongTaskClickListener)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] is Header) {
            HEADER_ITEM_VIEW_TYPE
        } else {
            TASK_ITEM_VIEW_TYPE
        }
    }

    private companion object {
        const val HEADER_ITEM_VIEW_TYPE = 0
        const val TASK_ITEM_VIEW_TYPE = 1
    }
}