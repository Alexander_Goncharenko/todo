package com.ncube.todo.presentation.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.apollographql.apollo.ApolloClient

@Suppress("UNCHECKED_CAST")
class MainActivityViewModelFactory(private val apolloClient: ApolloClient) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainActivityViewModel(apolloClient) as T
    }
}