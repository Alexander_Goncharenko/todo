package com.ncube.todo.presentation.main.adapter.model

data class Header(val titleStringRes: Int)