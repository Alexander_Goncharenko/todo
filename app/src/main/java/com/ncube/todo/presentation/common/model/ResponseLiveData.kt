package com.ncube.todo.presentation.common.model

import androidx.lifecycle.MutableLiveData
import com.ncube.todo.domain.model.common.Response
import com.ncube.todo.domain.model.common.Status

class ResponseLiveData<T> : MutableLiveData<Response<T>>() {

    val isLoading: Boolean
        get() {
            return value?.status == Status.LOADING
        }
}