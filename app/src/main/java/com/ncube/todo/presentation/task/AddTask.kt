package com.ncube.todo.presentation.task

import android.app.Activity
import android.content.Intent
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.ncube.todo.domain.model.common.Status
import com.ncube.todo.presentation.utils.observe
import kotlinx.android.synthetic.main.ac_task.*

interface AddTask {

    fun TaskActivity.setupAddTaskResponse() {
        viewModel.addTaskResponse.observe(this) {
            when (it?.status) {
                Status.LOADING -> {
                    pbContent.visibility = View.VISIBLE
                    tvTitle.isEnabled = false
                    tvNote.isEnabled = false
                    btnAdd.isEnabled = false
                }
                Status.SUCCESS -> {
                    val intent = Intent()
                    intent.putExtra(TaskActivity.ARG_TASK, it.data)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
                else -> {
                    pbContent.visibility = View.GONE
                    tvTitle.isEnabled = true
                    tvNote.isEnabled = true
                    btnAdd.isEnabled = true
                    Snackbar.make(clTaskContainer, it!!.error!!.message!!, Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun TaskActivity.setupAddMode() {
        ibDelete.visibility = View.GONE
        divider.visibility = View.GONE
        btnAdd.visibility = View.VISIBLE

        btnAdd.setOnClickListener {
            viewModel.addTask(tvTitle.text.toString(), tvNote.text.toString())
        }
    }

}