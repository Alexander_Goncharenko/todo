package com.ncube.todo.presentation.task

import CreateTaskMutation
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.rx2.Rx2Apollo
import com.ncube.todo.domain.model.common.Response
import com.ncube.todo.domain.model.task.CreateTaskMapper
import com.ncube.todo.domain.model.task.Task
import com.ncube.todo.presentation.common.BaseViewModel
import com.ncube.todo.presentation.common.model.ResponseLiveData
import com.ncube.todo.presentation.utils.applySchedulers

class TaskActivityViewModel(private val apolloClient: ApolloClient) : BaseViewModel() {

    var task: Task? = null

    val addTaskResponse = ResponseLiveData<Task>()

    fun addTask(title: String, note: String?) {
        val mutation = CreateTaskMutation
            .builder()
            .name(title)
            .note(note)
            .isDone(false)
            .build()
        val apolloPrefetch = apolloClient.mutate(mutation)

        addTaskResponse.value = Response.loading()

        Rx2Apollo
            .from(apolloPrefetch)
            .map { if (it.hasErrors()) error(it.errors().first().message()!!) else it.data()?.createTask() } //todo make extension function to handle errors with apollo
            .map(CreateTaskMapper())
            .applySchedulers()
            .subscribe(
                { addTaskResponse.value = Response.success(it) },
                { addTaskResponse.value = Response.error(it) }
            )
            .registerDisposable()
    }
}